GridEye (AMG88) thermal sensor breadboard and small example to read in thermal data and display it on an OLED (SSD1331)

See http://www.lemmini.de/GridEye/GridEye.html

---------------------------------------------------------------
Copyright 2015 Volker Oth - VolkerOth(at)gmx.de

Licensed under the Creative Commons Attribution 4.0 license
http://creativecommons.org/licenses/by/4.0/

Everything in this repository is distributed on an "AS IS" BASIS, WITHOUT
WARRANTIES OF ANY KIND, either express or implied.