/** SSD1331 OLED
	Communication through 4-line SPI mode
	LPC82x ARM Cortex M0+
	----------------------------------------------------------
	Copyright 2017 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

#ifndef SSD1331_H
#define SSD1331_H

#include "ssd1331_cnf.h"
#include "fonts.h"

/* SSD1331 system commands */
#define SSD1331_CMD_COL_ADR         0x15 ///! Set column start and end address. Two parameter bytes [0..95]
#define SSD1331_CMD_ROW_ADR         0x75 ///! Set row start and end address. Two parameter bytes [0..63]
#define SSD1331_CMD_CONTRASTA       0x81 ///! Set contrast for color A segment. One parameter byte [0..255]
#define SSD1331_CMD_CONTRASTB       0x82 ///! Set contrast for color B segment. One parameter byte [0..255]
#define SSD1331_CMD_CONTRASTC       0x83 ///! Set contrast for color C segment. One parameter byte [0..255]
#define SSD1331_CMD_ATTENTUATION    0x87 ///! Set master current attentuation factor. One parameter byte [0..15]
#define SSD1331_CMD_PRECHARGEA      0x8A ///! Set 2nd precharge speed for color A. One parameter byte [0..255]
#define SSD1331_CMD_PRECHARGEB      0x8B ///! Set 2nd precharge speed for color B. One parameter byte [0..255]
#define SSD1331_CMD_PRECHARGEC      0x8C ///! Set 2nd precharge speed for color C. One parameter byte [0..255]
#define SSD1331_CMD_REMAP           0xA0 ///! Set driver remap and color depth. One parameter byte [bitmask]
#define SSD1331_CMD_DISP_OFS_ROW    0xA1 ///! Set display start line register by Row. One parameter byte [0..63]
#define SSD1331_CMD_DISP_OFS_COL    0xA2 ///! Set vertical offset by Column. One parameter byte [0..63]
#define SSD1331_CMD_DISP_NORMAL     0xA4 ///! Set normal display mode. No parameter.
#define SSD1331_CMD_DISP_ALL_ON     0xA5 ///! Set entire display on. No parameter.
#define SSD1331_CMD_DISP_ALL_OFF 	0xA6 ///! Set entire display off. No parameter.
#define SSD1331_CMD_DISP_INVERT 	0xA7 ///! Set inverse display mode. No parameter.
#define SSD1331_CMD_MULTIPLEX       0xA8 ///! Set MUX ratio to N+1 Mux. One parameter byte [15..63]
#define SSD1331_CMD_DIM_MODE        0xAB ///! Set MUX ratio to N+1 Mux. 5 parameter bytes [0],[0..255]x3,[0..31]
#define SSD1331_CMD_MASTER_CONFIG   0xAD ///! Set Master configuration. One parameter byte [142..143]
#define SSD1331_CMD_DISP_DIM        0xAC ///! Display ON in dim mode. No parameter.
#define SSD1331_CMD_DISP_OFF        0xAE ///! Display OFF (sleep mode). No parameter.
#define SSD1331_CMD_DISP_ON         0xAF ///! Display ON in normal mode. No parameter.
#define SSD1331_CMD_POWER_MODE      0xB0 ///! Enable/disable power save mode. One parameter byte [0b0b/0x1a]
#define SSD1331_CMD_PERIOD_ADJUST   0xB1 ///! Set phase 1/2 period in clocks. One parameter byte with two nibbles.
#define SSD1331_CMD_CLOCKDIV        0xB3 ///! Set clock divider. One parameter byte.
#define SSD1331_CMD_GRAYSCALE_TABLE 0xB8 ///! Define pulse widths of GS1 to GS63 in terms of DCLK. 32 parameter bytes.
#define SSD1331_CMD_GRAYSCALE_RESET 0xB9 ///! Reset grayscale table to built in linear table. No parameter.
#define SSD1331_CMD_PRECHARGE_LEVEL 0xBB ///! Set precharge voltage level. One parameter byte [0..31]<<1
#define SSD1331_CMD_NOP             0xBC ///! No operation. 0xBD and 0xE3 work as well. No parameter.
#define SSD1331_CMD_VCOMH 			0xBE ///! Set COM deselect voltage level. One parameter byte [0..31]<<1
#define SSD1331_CMD_LOCK            0xFD ///! Lock/Unlock OLED driver. One parameter byte [18/22].

/* SSD1331 gfx commands */
#define SSD1331_CMD_DRAW_LINE       0x21 ///! Draw Line. 7 parameter bytes.
#define SSD1331_CMD_DRAW_RECT       0x22 ///! Draw rectangle. 10 parameter bytes.
#define SSD1331_CMD_COPY_RECT       0x23 ///! Copy rectangular area. 6 parameter bytes.
#define SSD1331_CMD_DIM_RECT        0x24 ///! Dim rectangular area. 4 parameter bytes.
#define SSD1331_CMD_CLEAR_RECT      0x25 ///! Clear rectangular area. 4 parameter bytes.
#define SSD1331_CMD_FILL            0x26 ///! Fill enable/disable. 1 parameter byte.
#define SSD1331_CMD_SCROLL          0x27 ///! Define continuous scrolling. 5 parameter bytes.
#define SSD1331_CMD_SCROLL_DISABLE  0x2E ///! Deactivate scrolling. No parameter.
#define SSD1331_CMD_SCROLL_ENABLE   0x2F ///! Activate scrolling. No parameter.


/* Bitmask defines for SSD1331_CMD_REMAP */
#define SSD1331_REMAP_H_INC         0x00     ///! Increment horizontal address
#define SSD1331_REMAP_V_INC         (1UL<<0) ///! Increment vertical address
#define SSD1331_REMAP_FLIP_COL      (1UL<<1) ///! Flip column mapping
#define SSD1331_REMAP_BGR           (1UL<<2) ///! Change to BGR mapping
#define SSD1331_REMAP_COM_LR        (1UL<<3) ///! COM pins left/right remap
#define SSD1331_REMAP_COM_SCAN_DIR  (1UL<<4) ///! COM scan direction remap
#define SSD1331_REMAP_COM_SPLIT_DIS 0x00     ///! Disable odd/even COM pin splitting
#define SSD1331_REMAP_COM_SPLIT_EN  (1UL<<5) ///! Enable odd/even COM pin splitting
#define SSD1331_REMAP_COLOR_256     0x00     ///! 256 color format
#define SSD1331_REMAP_COLOR_65K     (1UL<<6) ///! 65k format 1
#define SSD1331_REMAP_COLOR_65K_2   (2UL<<6) ///! 65k format 2


/* Color defines */
#define SSD1331_COLOR_WHITE			0xFFFF
#define SSD1331_COLOR_BLACK			0x0000
#define SSD1331_COLOR_RED			0xF800
#define SSD1331_COLOR_GREEN			0x07E0
#define SSD1331_COLOR_GREEN2		0xB723
#define SSD1331_COLOR_BLUE			0x001F
#define SSD1331_COLOR_BLUE2			0x051D
#define SSD1331_COLOR_YELLOW		0xFFE0
#define SSD1331_COLOR_ORANGE		0xFBE4
#define SSD1331_COLOR_CYAN			0x07FF
#define SSD1331_COLOR_MAGENTA		0xA254
#define SSD1331_COLOR_GRAY			0x7BEF
#define SSD1331_COLOR_BROWN			0xBBCA

/* Automatic defines */
#if SSD1331_LANDSCAPE_MODE
#define SSD1331_SIZE_X 96
#define SSD1331_SIZE_Y 64
#else
#define SSD1331_SIZE_X 64
#define SSD1331_SIZE_Y 96
#endif

/* Prototypes */
extern void SSD1331_WriteCommand(u8 cmd);
extern void SSD1331_WriteCommands(u8 *commands, u16 len);

extern void SSD1331_SetArea(u16 x1, u16 y1, u16 x2, u16 y2);
extern void SSD1331_DrawPixel(u16 x, u16 y, u16 color);
extern void SSD1331_SetFullScreen(void);
extern void SSD1331_FillRect(u16 x1, u16 y1, u16 x2, u16 y2, u16 color);
extern void SSD1331_FillRectG(u16 x1, u16 y1, u16 x2, u16 y2, u16 color1, u16 color2);
extern void SSD1331_FillScreen(u16 color);
extern void SSD1331_FillScreenG(u16 color1, u16 color2);
extern void SSD1331_BlitRect(u16 x1, u16 y1, u16 x2, u16 y2, u16 *data);
extern void SSD1331_DrawStr(u16 x, u16 y, char* str, font_t *font, u16 fg_col, u16 bg_col);
extern void SSD1331_DrawStrG(u16 x, u16 y, char* str, font_t *font, u16 fg_col1, u16 fg_col2, u16 bg_col1, u16 bg_col2);
extern void SSD1331_Init(void);

#endif
