/** AMG88 Library
	Definitions for AMG88/Grid-Eye library
	LPC824 ARM Cortex M0+
	----------------------------------------------------------
	Copyright 2017 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

#ifndef AMG88_H
#define AMG88_H

#include "amg88_cnf.h"

#define AMG88_I2C_ADDRESS_LO 0x68
#define AMG88_I2C_ADDRESS_HI 0x69

#define AMG88_SIZE_X   8
#define AMG88_SIZE_Y   8
#define AMG88_PIXEL_NUM (AMG88_SIZE_X*AMG88_SIZE_Y)

/* I2C registers */
#define AMG88_CMD_PCTL  0x00 ///! Power control - Set operating mode(Normal, Sleep etc.) [R/W]
#define AMG88_CMD_RST   0x01 ///! Software Reset [W]
#define AMG88_CMD_FPSC  0x02 ///! Frame rate control [R/W]
#define AMG88_CMD_INTC  0x03 ///! Interrupt control [R/W]
#define AMG88_CMD_STAT  0x04 ///! Status register [R]
#define AMG88_CMD_SCLR  0x05 ///! Status clear register [W]
#define AMG88_CMD_AVE   0x07 ///! Moving average output mode [W]
#define AMG88_CMD_INTHL 0x08 ///! Interrupt upper value low [R/W]
#define AMG88_CMD_INTHH 0x09 ///! Interrupt upper value high [R/W]
#define AMG88_CMD_INTLL 0x0A ///! Interrupt lower value low [R/W]
#define AMG88_CMD_INTLH 0x0B ///! Interrupt lower value high [R/W]
#define AMG88_CMD_IHYSL 0x0C ///! Interrupt hysteresis value low [R/W]
#define AMG88_CMD_IHYSH 0x0D ///! Interrupt hysteresis value high [R/W]
#define AMG88_CMD_TTHL  0x0E ///! Thermistor output value low [R]
#define AMG88_CMD_TTHH  0x0F ///! Thermistor output value high [R]
#define AMG88_CMD_INT0  0x10 ///! Pixel 1-8 interrupt result [R]
#define AMG88_CMD_INT1  0x11 ///! Pixel 9-16 interrupt result [R]
#define AMG88_CMD_INT2  0x12 ///! Pixel 17-24 interrupt result [R]
#define AMG88_CMD_INT3  0x13 ///! Pixel 25-32 interrupt result [R]
#define AMG88_CMD_INT4  0x14 ///! Pixel 33-40 interrupt result [R]
#define AMG88_CMD_INT5  0x15 ///! Pixel 41-48 interrupt result [R]
#define AMG88_CMD_INT6  0x16 ///! Pixel 49-56 interrupt result [R]
#define AMG88_CMD_INT7  0x17 ///! Pixel 57-64 interrupt result [R]

#define AMG88_CMD_AVENB 0x1F ///! Needed to enable moving average

#define AMG88_CMD_T01L  0x80 ///! Pixel 1 output value low [R]
#define AMG88_CMD_T01H  0x81 ///! Pixel 1 output value high [R]
#define AMG88_CMD_T02L  0x82 ///! Pixel 2 output value low [R]
#define AMG88_CMD_T02H  0x83 ///! Pixel 2 output value high [R]
#define AMG88_CMD_T03L  0x84 ///! Pixel 3 output value low [R]
#define AMG88_CMD_T03H  0x85 ///! Pixel 3 output value high [R]
#define AMG88_CMD_T04L  0x86 ///! Pixel 4 output value low [R]
#define AMG88_CMD_T04H  0x87 ///! Pixel 4 output value high [R]
#define AMG88_CMD_T05L  0x88 ///! Pixel 5 output value low [R]
#define AMG88_CMD_T05H  0x89 ///! Pixel 5 output value high [R]
#define AMG88_CMD_T06L  0x8A ///! Pixel 6 output value low [R]
#define AMG88_CMD_T06H  0x8B ///! Pixel 6 output value high [R]
#define AMG88_CMD_T07L  0x8C ///! Pixel 7 output value low [R]
#define AMG88_CMD_T07H  0x8D ///! Pixel 7 output value high [R]
#define AMG88_CMD_T08L  0x8E ///! Pixel 8 output value low [R]
#define AMG88_CMD_T08H  0x8F ///! Pixel 8 output value high [R]

#define AMG88_CMD_T09L  0x90 ///! Pixel 9 output value low [R]
#define AMG88_CMD_T09H  0x91 ///! Pixel 9 output value high [R]
#define AMG88_CMD_T10L  0x92 ///! Pixel 10 output value low [R]
#define AMG88_CMD_T10H  0x93 ///! Pixel 10 output value high [R]
#define AMG88_CMD_T11L  0x94 ///! Pixel 11 output value low [R]
#define AMG88_CMD_T11H  0x95 ///! Pixel 11 output value high [R]
#define AMG88_CMD_T12L  0x96 ///! Pixel 12 output value low [R]
#define AMG88_CMD_T12H  0x97 ///! Pixel 12 output value high [R]
#define AMG88_CMD_T13L  0x98 ///! Pixel 13 output value low [R]
#define AMG88_CMD_T13H  0x99 ///! Pixel 13 output value high [R]
#define AMG88_CMD_T14L  0x9A ///! Pixel 14 output value low [R]
#define AMG88_CMD_T14H  0x9B ///! Pixel 14 output value high [R]
#define AMG88_CMD_T15L  0x9C ///! Pixel 15 output value low [R]
#define AMG88_CMD_T15H  0x9D ///! Pixel 15 output value high [R]
#define AMG88_CMD_T16L  0x9E ///! Pixel 15 output value low [R]
#define AMG88_CMD_T16H  0x9F ///! Pixel 15 output value high [R]

#define AMG88_CMD_T17L  0xA0 ///! Pixel 17 output value low [R]
#define AMG88_CMD_T17H  0xA1 ///! Pixel 17 output value high [R]
#define AMG88_CMD_T18L  0xA2 ///! Pixel 18 output value low [R]
#define AMG88_CMD_T18H  0xA3 ///! Pixel 18 output value high [R]
#define AMG88_CMD_T19L  0xA4 ///! Pixel 19 output value low [R]
#define AMG88_CMD_T19H  0xA5 ///! Pixel 19 output value high [R]
#define AMG88_CMD_T20L  0xA6 ///! Pixel 20 output value low [R]
#define AMG88_CMD_T20H  0xA7 ///! Pixel 20 output value high [R]
#define AMG88_CMD_T21L  0xA8 ///! Pixel 21 output value low [R]
#define AMG88_CMD_T21H  0xA9 ///! Pixel 21 output value high [R]
#define AMG88_CMD_T22L  0xAA ///! Pixel 22 output value low [R]
#define AMG88_CMD_T22H  0xAB ///! Pixel 22 output value high [R]
#define AMG88_CMD_T23L  0xAC ///! Pixel 23 output value low [R]
#define AMG88_CMD_T23H  0xAD ///! Pixel 23 output value high [R]
#define AMG88_CMD_T24L  0xAE ///! Pixel 24 output value low [R]
#define AMG88_CMD_T24H  0xAF ///! Pixel 24 output value high [R]

#define AMG88_CMD_T25L  0xB0 ///! Pixel 25 output value low [R]
#define AMG88_CMD_T25H  0xB1 ///! Pixel 25 output value high [R]
#define AMG88_CMD_T26L  0xB2 ///! Pixel 26 output value low [R]
#define AMG88_CMD_T26H  0xB3 ///! Pixel 26 output value high [R]
#define AMG88_CMD_T27L  0xB4 ///! Pixel 27 output value low [R]
#define AMG88_CMD_T27H  0xB5 ///! Pixel 27 output value high [R]
#define AMG88_CMD_T28L  0xB6 ///! Pixel 28 output value low [R]
#define AMG88_CMD_T28H  0xB7 ///! Pixel 28 output value high [R]
#define AMG88_CMD_T29L  0xB8 ///! Pixel 29 output value low [R]
#define AMG88_CMD_T29H  0xB9 ///! Pixel 29 output value high [R]
#define AMG88_CMD_T30L  0xBA ///! Pixel 30 output value low [R]
#define AMG88_CMD_T30H  0xBB ///! Pixel 30 output value high [R]
#define AMG88_CMD_T31L  0xBC ///! Pixel 31 output value low [R]
#define AMG88_CMD_T31H  0xBD ///! Pixel 31 output value high [R]
#define AMG88_CMD_T32L  0xBE ///! Pixel 32 output value low [R]
#define AMG88_CMD_T32H  0xBF ///! Pixel 32 output value high [R]

#define AMG88_CMD_T33L  0xC0 ///! Pixel 33 output value low [R]
#define AMG88_CMD_T33H  0xC1 ///! Pixel 33 output value high [R]
#define AMG88_CMD_T34L  0xC2 ///! Pixel 34 output value low [R]
#define AMG88_CMD_T34H  0xC3 ///! Pixel 34 output value high [R]
#define AMG88_CMD_T35L  0xC4 ///! Pixel 35 output value low [R]
#define AMG88_CMD_T35H  0xC5 ///! Pixel 35 output value high [R]
#define AMG88_CMD_T36L  0xC6 ///! Pixel 36 output value low [R]
#define AMG88_CMD_T36H  0xC7 ///! Pixel 36 output value high [R]
#define AMG88_CMD_T37L  0xC8 ///! Pixel 37 output value low [R]
#define AMG88_CMD_T37H  0xC9 ///! Pixel 37 output value high [R]
#define AMG88_CMD_T38L  0xCA ///! Pixel 38 output value low [R]
#define AMG88_CMD_T38H  0xCB ///! Pixel 38 output value high [R]
#define AMG88_CMD_T39L  0xCC ///! Pixel 39 output value low [R]
#define AMG88_CMD_T39H  0xCD ///! Pixel 39 output value high [R]
#define AMG88_CMD_T40L  0xCE ///! Pixel 40 output value low [R]
#define AMG88_CMD_T40H  0xCF ///! Pixel 40 output value high [R]

#define AMG88_CMD_T41L  0xD0 ///! Pixel 41 output value low [R]
#define AMG88_CMD_T41H  0xD1 ///! Pixel 41 output value high [R]
#define AMG88_CMD_T42L  0xD2 ///! Pixel 42 output value low [R]
#define AMG88_CMD_T42H  0xD3 ///! Pixel 42 output value high [R]
#define AMG88_CMD_T43L  0xD4 ///! Pixel 43 output value low [R]
#define AMG88_CMD_T43H  0xD5 ///! Pixel 43 output value high [R]
#define AMG88_CMD_T44L  0xD6 ///! Pixel 44 output value low [R]
#define AMG88_CMD_T44H  0xD7 ///! Pixel 44 output value high [R]
#define AMG88_CMD_T45L  0xD8 ///! Pixel 45 output value low [R]
#define AMG88_CMD_T45H  0xD9 ///! Pixel 45 output value high [R]
#define AMG88_CMD_T46L  0xDA ///! Pixel 46 output value low [R]
#define AMG88_CMD_T46H  0xDB ///! Pixel 46 output value high [R]
#define AMG88_CMD_T47L  0xDC ///! Pixel 47 output value low [R]
#define AMG88_CMD_T47H  0xDD ///! Pixel 47 output value high [R]
#define AMG88_CMD_T48L  0xDE ///! Pixel 48 output value low [R]
#define AMG88_CMD_T48H  0xDF ///! Pixel 48 output value high [R]

#define AMG88_CMD_T49L  0xE0 ///! Pixel 49 output value low [R]
#define AMG88_CMD_T49H  0xE1 ///! Pixel 49 output value high [R]
#define AMG88_CMD_T50L  0xE2 ///! Pixel 50 output value low [R]
#define AMG88_CMD_T50H  0xE3 ///! Pixel 50 output value high [R]
#define AMG88_CMD_T51L  0xE4 ///! Pixel 51 output value low [R]
#define AMG88_CMD_T51H  0xE5 ///! Pixel 51 output value high [R]
#define AMG88_CMD_T52L  0xE6 ///! Pixel 52 output value low [R]
#define AMG88_CMD_T52H  0xE7 ///! Pixel 52 output value high [R]
#define AMG88_CMD_T53L  0xE8 ///! Pixel 53 output value low [R]
#define AMG88_CMD_T53H  0xE9 ///! Pixel 53 output value high [R]
#define AMG88_CMD_T54L  0xEA ///! Pixel 54 output value low [R]
#define AMG88_CMD_T54H  0xEB ///! Pixel 54 output value high [R]
#define AMG88_CMD_T55L  0xEC ///! Pixel 55 output value low [R]
#define AMG88_CMD_T55H  0xED ///! Pixel 55 output value high [R]
#define AMG88_CMD_T56L  0xEE ///! Pixel 56 output value low [R]
#define AMG88_CMD_T56H  0xEF ///! Pixel 56 output value high [R]

#define AMG88_CMD_T57L  0xF0 ///! Pixel 57 output value low [R]
#define AMG88_CMD_T57H  0xF1 ///! Pixel 57 output value high [R]
#define AMG88_CMD_T58L  0xF2 ///! Pixel 58 output value low [R]
#define AMG88_CMD_T58H  0xF3 ///! Pixel 58 output value high [R]
#define AMG88_CMD_T59L  0xF4 ///! Pixel 59 output value low [R]
#define AMG88_CMD_T59H  0xF5 ///! Pixel 59 output value high [R]
#define AMG88_CMD_T60L  0xF6 ///! Pixel 60 output value low [R]
#define AMG88_CMD_T60H  0xF7 ///! Pixel 60 output value high [R]
#define AMG88_CMD_T61L  0xF8 ///! Pixel 61 output value low [R]
#define AMG88_CMD_T61H  0xF9 ///! Pixel 61 output value high [R]
#define AMG88_CMD_T62L  0xFA ///! Pixel 62 output value low [R]
#define AMG88_CMD_T62H  0xFB ///! Pixel 62 output value high [R]
#define AMG88_CMD_T63L  0xFC ///! Pixel 63 output value low [R]
#define AMG88_CMD_T63H  0xFD ///! Pixel 63 output value high [R]
#define AMG88_CMD_T64L  0xFE ///! Pixel 64 output value low [R]
#define AMG88_CMD_T64H  0xFF ///! Pixel 64 output value high [R]

/* Power control [R/W] */
#define AMG88_PCTL_NORMAL    0x00 ///! Normal Mode (default)
#define AMG88_PCTL_SLEEP     0x10 ///! Sleep Mode
#define AMG88_PCTL_STBY_60   0x20 ///! Stand-by with 60 second interruption
#define AMG88_PCTL_STBY_10   0x21 ///! Stand-by with 10 second interruption

/* Reset control [W] */
#define AMG88_RST_FLAG_RESET 0x30 ///! Clear all interrupt and status flags
#define AMG88_RST_INIT_RESET 0x3F ///! Return to initial settings

/* Frame Rate Control [R/W] */
#define AMG88_FPSC_10FP      0x00 ///! 10 frames per second (default)
#define AMG88_FPSC_1FPS      0x01 ///! 1 frame per second

/* Interrupt Control [R/W] */
#define AMG88_INTC_OUT_DISBLE 0x00     ///! Interrupt output disabled (default)
#define AMG88_INTC_OUT_ENABLE (1UL<<0) ///! Interrupt output enabled
#define AMG88_INTC_MODE_DIFF  0x00     ///! Difference interrupt mode
#define AMG88_INTC_MODE_ABS   (1UL<<1) ///! Absolute value interrupt mode

/* Status register [R] */
#define AMG88_STAT_INTF       (1UL<<1)   ///! Accumulated interrupt flag
#define AMG88_STAT_IRS_OVF    (1UL<<2)   ///! Temperature output overflow
#define AMG88_STAT_THERM_OVF  (1UL<<3)   ///! Thermistor temperature output overflow

/* Status clear register [W] */
#define AMG88_SCLR_INTF       (1UL<<1)   ///! Accumulated interrupt flag
#define AMG88_SCLR_IRS_OVF    (1UL<<2)   ///! Temperature output overflow flag
#define AMG88_SCLR_THERM_OVF  (1UL<<3)   ///! Thermistor temperature output overflow flag

/* Average mode register [W] */
#define AMG88_AVE_NONE        0x00       ///! Moving average mode disabled
#define AMG88_AVE_TWICE       (1UL<<5)   ///! Moving average mode enabled


extern u16 amg88_data[AMG88_PIXEL_NUM];
extern s16 amg88_temp[AMG88_PIXEL_NUM];

/* Prototypes */
extern u8 AMG88_StartRead(u8 reg, u8 *buffer, u8 len);
extern s16 AMG88_GetThermistorTemp(void);
extern s16 AMG88_GetTempMax(void) ;
extern s16 AMG88_GetTempMin(void);
extern void AMG88_ReadData(void);
extern void AMG88_ConvertData(void);
extern void AMG88_WaitForCompletion(void);
#endif
