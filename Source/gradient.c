/** RGB Gradient library
	Helper function for 16bit (RGB 5-6-5) and 24bit graphics display
	LPC82x ARM Cortex M0+
	----------------------------------------------------------
	Copyright 2015 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

#include "global.h"
#include "gradient.h"

void Gradient_Create16(u16 col1, u16 col2, u16 steps, gradient_t *grad) {
	grad->r0 = (col1>>11)&31;
	grad->g0 = (col1 >>5)&63;
	grad->b0 = (col1 & 31);
	grad->dr = (((s32)(col2&0xf800) - (s32)(col1&0xf800))/4)/steps;        // ((col2>>11) - (col1>>11))*512;
	grad->dg = (((s32)(col2&0x7e0) - (s32)(col1&0x7e0))<<4)/steps;         // (((col2>>5) &63)- ((col1 >>5))&63))*512;
	grad->db = (((s32)(col2 & 31) - (s32)grad->b0)*512)/steps;
}

u16 Gradient_GetColor16(gradient_t *grad, u16 step) {
	u16 r = (u16)((s32)grad->r0 + ((s32)grad->dr*step)/512);
	u16 g = (u16)((s32)grad->g0 + ((s32)grad->dg*step)/512);
	u16 b = (u16)((s32)grad->b0 + ((s32)grad->db*step)/512);
	return ((r&31)<<11)|((g&63)<<5)|(b&31);
}

void Gradient_Create24(u32 col1, u32 col2, u16 steps, gradient_t *grad) {
	u8 r,g,b;
	grad->r0 = (col1>>16)&0xff;
	grad->g0 = (col1 >>8)&0xff;
	grad->b0 = (col1 & 0xff);
	r = (col2>>16)&0xff;
	g = (col2 >>8)&0xff;
	b = (col2 & 0xff);
	grad->dr = (((s32)r - (s32)grad->r0)*512)/steps;   //(((s32)(col2&0xff0000) - (s32)(col1&0xff0000))/128)/steps; // ((col2>>16) - (col1>>16))*512;
	grad->dg = (((s32)g - (s32)grad->g0)*512)/steps;   // (((s32)(col2&0xff00) - (s32)(col1&0xff00))<<1)/steps;     // (((col2>>8) &0xff)- ((col1 >>8))&0xff))*512;
	grad->db = (((s32)b - (s32)grad->b0)*512)/steps;
}

u32 Gradient_GetColor24(gradient_t *grad, u16 step) {
	u32 r = (u32)((s32)grad->r0 + ((s32)grad->dr*step)/512);
	u32 g = (u32)((s32)grad->g0 + ((s32)grad->dg*step)/512);
	u32 b = (u32)((s32)grad->b0 + ((s32)grad->db*step)/512);
	return ((r&0xff)<<16)|((g&0xff)<<8)|(b&0xff);
}
