/** SSD1331 OLED Library - Configuration
	Communication through 4-line SPI mode
	LPC82x ARM Cortex M0+
	----------------------------------------------------------
	Copyright 2017 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

#ifndef SSD1331_CNF_H
#define SSD1331_CNF_H

/** define all interrupt levels here (0 is the highest, 31 the lowest prio) */

#define SSD1331_SPI_CH          SPI_CH_OLED      ///! SPI channel used for communication
#define SSD1331_DC_PIN          PIN_0_1          ///! PIN used as D/C pin
#define SSD1331_RESET_PIN       PIN_0_0          ///! PIN used as reset pin

#define SSD1331_LANDSCAPE_MODE   0               ///! default is landscape mode : 96x64, portrait is 64x96
#define SSD1331_FLIPPED_MODE_X   1               ///! set to 1 for flipped x mode
#define SSD1331_FLIPPED_MODE_Y   0               ///! set to 1 for flipped y mode

#define SSD1331_MAX_TRANSFER     0x400           ///! Maximum amount of bytes that can be transferred in one SPI/DMA transfer

#endif
