/** I2C library - Configuration
	Configuration of I2C peripheral
	LPC82x ARM Cortex M0+
	----------------------------------------------------------
	Copyright 2012 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

#include "global.h"
#include "sys.h"
#include "i2c.h"
#include "prc.h"

/** Assign physical I2C devices to virtual devices - no need to use them all */
const LPC_I2C_Type *i2c_device_cfg[I2C_VIRTUAL_DEVICE_NUM] = {
	/* device0 */ LPC_I2C0,
};

/** Timing profiles */
const i2c_timing_cfg_t i2c_timing_profile_cfg[I2C_TIMING_PROFILES_NUM] = {
	/* profile 0 */ { 100000 /* 100kHz */, 0x4000 /* 50% */}, /* 100kHz profile */
	/* profile 1 */ { 400000 /* 400kHz */, 0x2e14 /* 36% */}  /* 400kHz profile */
};

/* Note
   In normal mode (100kHz), the low phase should be the same as the high phase
   In fast mode (400kHz), the relation between low and high should be 16/9
   -> the duty cycle of the high phase is 9/25 = 36% */

/** Communication channel configuration  */
const i2c_channel_cfg_t i2c_channel_cfg[I2C_CHANNEL_CONFIG_NUM] = {
			 /* device id      slave address   address size         timing profile   retries */
	/* ch0 */ { 0 /* dev0 */,  0x69,           I2C_ADR_SIZE_8BIT,   1 /* prof1 */,   3 }, /* AMG88 */
	// /* ch0 */ { 0 /* dev0 */,  0x70,           I2C_ADR_SIZE_8BIT,   1 /* prof1 */,   3 }, /* HT16K33 */
	// /* ch0 */ { 0 /* dev0 */,  0x50,           I2C_ADR_SIZE_16BIT,  1 /* prof1 */,   3 }, /* AT24C256 EEPROM DATA */
	// /* ch1 */ { 0 /* dev0 */,  0x50,           I2C_ADR_SIZE_16BIT,  1 /* prof1 */,   0 }, /* AT24C256 EEPROM ACK POLL */
};
