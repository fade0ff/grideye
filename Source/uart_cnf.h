/** UART library - Configuration
	Communication via asynchronous serial interface (UART)
	LPC82x ARM Cortex M0+
	----------------------------------------------------------
	Copyright 2012 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

#ifndef UART_CNF_H
#define UART_CNF_H


#define UART_MLINK 0

/** Number of actually used devices as configured in uart_config_ptr */
#define UART_NUM_DEVICES         1

/** Global fractional divider setup
    Note: DIV must be always configured to 255 (->256)
    MULT can be configured 0..255
        baudrate =  SYS_CLK_UART_FRQ/((1+(MULT/256))*16*BRGVAL)
                 =  SYS_CLK_UART_FRQ*256/((256 + MULT)*16*BRGVAL)
                 =  SYS_CLK_UART_FRQ*16/((256 + MULT)*BRGVAL)
    So you must select MULT in a a way that you can reach your baudrate with a BRGVAL [1..0x10000]
        BRGVAL / (256 + MULT) = SYS_CLK_UART_FRQ/baudrate/4096
    For 72MHz UART frequency, 244 is a good value:
        BRGVAL =  5 for 460800 Baud
        BRGVAL = 20 for 115200 Baud
        ...
        BRGVAL = 240 for 9600 Baud

    For 12MHz, 115200 Baud don't seem to be possible exactly,
    but with MULT=22 at least 115107.914 Baud are possible (BRGVAL=6)
    Even 9600Baud are problematic with 12MHz:
        BRGVAL / (256 + MULT) = 12e6/9600/4096 = 0.30517578125 = 625/2048
    As a denominator of 2048 is not possible with a maximum MULT of 255,
    you can just try to find a MULT/BRGVAL values which result in a minimal error.
 */
#define UART_FRGCTRL_MULT_VAL  22 /*244*/

/** Use DMA or IRQ - Note that dma_cnf.h has to be changed in case of DMA selected! */
#define UART_USE_RX_DMA             1
#define UART_USE_TX_DMA             1

/** Size of RX buffer per channel - use power of two to get optimized code */
#define UART_RX_BUFSIZE        128

#endif
