/** AMG88 Library
	Definitions for AMG88/Grid-Eye library Configuration
	LPC824 ARM Cortex M0+
	----------------------------------------------------------
	Copyright 2017 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

#ifndef AMG88_CNF_H
#define AMG88_CNF_H

#define AMG88_I2C_CH          I2C_CH_AMG88     ///! I2C channel used for communication

#endif
