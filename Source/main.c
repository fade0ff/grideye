/** GridEye
	Small example to read in thermal data from a GridEye sensor (AMG88) and display it on an OLED (SSD1331)

	----------------------------------------------------------
	Copyright 2017 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

#include "global.h"
#include "sys.h"
#include "prc.h"
#include "pin.h"
#include "priowrap.h"
#include "dma.h"
#include "i2c.h"
#include "spi.h"
#include "uart.h"
#include "mrt.h"
#include "gradient.h"
#include "amg88.h"
#include "ssd1331.h"
#include "systick.h"
#include "systime.h"
#include "system_LPC8xx.h"
#include <string.h>
#include <stdlib.h>


#define IR_SCALE_X 8  ///! Scale up the 8x8 IR image horizontally
#define IR_SCALE_Y 8  ///! Scale up the 8x8 IR image vertically

#define BGCOL1 RGB16(0,0,31)
#define BGCOL2 RGB16(0,0,8)

#define PAL_SIZE 107

#define GAIN_MAX 5

/// Infrared color table (close to FLIR's "Iron Bow")
const u16 pal[PAL_SIZE] = {
		RGB16(0,0,0   ),
		RGB16(0,0,4   ),
		RGB16(0,0,6   ),
		RGB16(0,0,8   ),
		RGB16(0,0,10  ),
		RGB16(0,0,11  ),
		RGB16(0,0,12  ),
		RGB16(1,0,13  ),
		RGB16(1,0,14  ),
		RGB16(2,0,14  ),
		RGB16(2,0,15  ),
		RGB16(3,0,16  ),
		RGB16(4,0,16  ),
		RGB16(5,0,17  ),
		RGB16(6,0,17  ),
		RGB16(7,0,17  ),
		RGB16(7,0,18  ),
		RGB16(8,0,18  ),
		RGB16(9,0,18  ),
		RGB16(10,0,18 ),
		RGB16(11,0,19 ),
		RGB16(12,0,19 ),
		RGB16(13,0,19 ),
		RGB16(14,0,19 ),
		RGB16(15,0,19 ),
		RGB16(16,0,19 ),
		RGB16(17,0,19 ),
		RGB16(18,0,19 ),
		RGB16(19,0,19 ),
		RGB16(20,0,19 ),
		RGB16(21,0,19 ),
		RGB16(21,0,18 ),
		RGB16(22,0,18 ),
		RGB16(22,1,18 ),
		RGB16(23,1,18 ),
		RGB16(23,2,18 ),
		RGB16(24,3,17 ),
		RGB16(24,4,16 ),
		RGB16(25,5,16 ),
		RGB16(25,6,15 ),
		RGB16(26,7,14 ),
		RGB16(26,8,13 ),
		RGB16(26,9,13 ),
		RGB16(26,10,12),
		RGB16(27,11,11),
		RGB16(27,12,9 ),
		RGB16(27,13,8 ),
		RGB16(27,14,7 ),
		RGB16(27,15,5 ),
		RGB16(28,16,4 ),
		RGB16(28,17,3 ),
		RGB16(28,18,2 ),
		RGB16(28,19,2 ),
		RGB16(28,19,1 ),
		RGB16(29,20,1 ),
		RGB16(29,21,1 ),
		RGB16(29,22,1 ),
		RGB16(29,23,1 ),
		RGB16(29,24,0 ),
		RGB16(29,25,0 ),
		RGB16(29,26,0 ),
		RGB16(29,27,0 ),
		RGB16(30,28,0 ),
		RGB16(30,29,0 ),
		RGB16(30,30,0 ),
		RGB16(30,31,0 ),
		RGB16(30,32,0 ),
		RGB16(30,33,0 ),
		RGB16(30,34,0 ),
		RGB16(30,35,0 ),
		RGB16(30,36,0 ),
		RGB16(30,37,0 ),
		RGB16(31,38,0 ),
		RGB16(31,39,0 ),
		RGB16(31,40,0 ),
		RGB16(31,42,0 ),
		RGB16(31,43,0 ),
		RGB16(31,44,0 ),
		RGB16(31,45,0 ),
		RGB16(31,46,0 ),
		RGB16(31,47,0 ),
		RGB16(31,48,0 ),
		RGB16(31,49,0 ),
		RGB16(31,50,0 ),
		RGB16(31,51,0 ),
		RGB16(31,51,1 ),
		RGB16(31,52,1 ),
		RGB16(31,53,1 ),
		RGB16(31,54,2 ),
		RGB16(31,55,3 ),
		RGB16(31,55,4 ),
		RGB16(31,56,5 ),
		RGB16(31,57,6 ),
		RGB16(31,57,8 ),
		RGB16(31,58,9 ),
		RGB16(31,59,11),
		RGB16(31,59,12),
		RGB16(31,60,14),
		RGB16(31,60,16),
		RGB16(31,60,18),
		RGB16(31,61,20),
		RGB16(31,61,22),
		RGB16(31,61,23),
		RGB16(31,62,25),
		RGB16(31,62,26),
		RGB16(31,63,28),
		RGB16(31,63,29)
};

#define draw_string(x,y,str,c1,c2) SSD1331_DrawStrG((x), (y), (str), &font_11x18, (c1), (c2), Gradient_GetColor16(&gb, y),Gradient_GetColor16(&gb, y+font_11x18.height));

gradient_t gb;
volatile u32 ms_counter;

s16 t_min, t_max, t_thermistor;

char str_buffer[16];
u8 tx_buffer[8];
u8 rx_buffer[64];
u8 adr = 0xff;

/** This buffer is supposed to hold two scaled up line of pixels */
u16 img_buffer[2][AMG88_SIZE_X*IR_SCALE_X*IR_SCALE_Y];

void int2str( int z, char* buffer ) {
	int i = 0;
	int j;
	char tmp;
	unsigned u;

	if( z < 0 ) {
		buffer[0] = '-';
		buffer++;
		u = -z;
	} else
		u = (unsigned)z;
	do {
		buffer[i++] = '0' + u % 10;
		u /= 10;
	} while( u > 0 );

	for( j = 0; j < i / 2; ++j ) {
		tmp = buffer[j];
		buffer[j] = buffer[i-j-1];
		buffer[i-j-1] = tmp;
	}
	buffer[i] = '\0';
}

/**
  Draw the IR scaled up images
  Uses amg88_temp, t_max, t_min
 */
void draw_ir(u16 x, u16 y) {
	int xs,ys_ofs,xd_ofs,yd_ofs,yd,scx,scy,idx,line_len;
	u16 color;
	u16 gain = (PAL_SIZE-1) / (t_max-t_min);
	if (gain > GAIN_MAX)
		gain = GAIN_MAX;
	idx = 0;
	yd = y;
	line_len = AMG88_SIZE_X*IR_SCALE_X; // size of the target line in pixels
	for (ys_ofs=0; ys_ofs<AMG88_PIXEL_NUM; ys_ofs+=AMG88_SIZE_X) {
		xd_ofs = 0;
		//for (xs=0; xs<AMG88_SIZE_X; xs++) {
		for (xs=AMG88_SIZE_X-1; xs>=0; xs--) {
			color = (u16)(amg88_temp[ys_ofs+xs] - t_min)*gain;
			color = PRC_ReverseByteOrderW(pal[color]); // OLED uses big endian byte order
			for (scx=0; scx<IR_SCALE_X; scx++)
				img_buffer[idx][xd_ofs+scx] = color;
			xd_ofs+=IR_SCALE_X;
		}
		// copy the line to scale up in Y direction
		yd_ofs = 0;
		for (scy=1; scy<IR_SCALE_Y; scy++, yd_ofs+=line_len)
			memcpy(&img_buffer[idx][yd_ofs+line_len], &img_buffer[idx][yd_ofs], line_len*2);
		// IR_SCALE_Y lines in buffer
		SSD1331_BlitRect(x, yd, (AMG88_SIZE_X*IR_SCALE_X)-1, yd+IR_SCALE_Y-1, img_buffer[idx]);
		yd += IR_SCALE_Y;
		idx = !idx; // flip index
	}
}

/**
  Draw the IR scaled up rotated image
  Uses amg88_temp, t_max, t_min
 */
void draw_ir_rot(u16 x, u16 y) {
	int xs,ys_ofs,xd_ofs,yd_ofs,yd,scx,scy,idx,line_len;
	u16 color;
	u16 gain = (PAL_SIZE-1) / (t_max-t_min);
	if (gain > GAIN_MAX)
		gain = GAIN_MAX;
	idx = 0;
	yd = y;
	line_len = AMG88_SIZE_X*IR_SCALE_X; // size of the target line in pixels
	for (xs=0; xs<AMG88_SIZE_X; xs++) {
	//for (xs=AMG88_SIZE_X-1; xs>=0; xs--) {
		xd_ofs = 0;
		for (ys_ofs=0; ys_ofs<AMG88_PIXEL_NUM; ys_ofs+=AMG88_SIZE_X) {
			color = (u16)(amg88_temp[ys_ofs+xs] - t_min)*gain;
			color = PRC_ReverseByteOrderW(pal[color]); // OLED uses big endian byte order
			for (scx=0; scx<IR_SCALE_X; scx++)
				img_buffer[idx][xd_ofs+scx] = color;
			xd_ofs+=IR_SCALE_X;
		}
		// copy the line to scale up in Y direction
		yd_ofs = 0;
		for (scy=1; scy<IR_SCALE_Y; scy++, yd_ofs+=line_len)
			memcpy(&img_buffer[idx][yd_ofs+line_len], &img_buffer[idx][yd_ofs], line_len*2);
		// IR_SCALE_Y lines in buffer
		SSD1331_BlitRect(x, yd, (AMG88_SIZE_X*IR_SCALE_X)-1, yd+IR_SCALE_Y-1, img_buffer[idx]);
		yd += IR_SCALE_Y;
		idx = !idx; // flip index
	}
}

/** 1ms low priority task
	@param dummy unused parameter needed for Prio_Wrap_Func_t
*/
void task_1ms(u32 dummy) {
	(void)dummy;

}

/** 10ms low priority task
	@param dummy unused parameter needed for Prio_Wrap_Func_t
*/
void task_10ms(u32 dummy) {
	(void)dummy;
}

/** 100ms low priority task
	@param dummy unused parameter needed for Prio_Wrap_Func_t
*/
void task_100ms(u32 dummy) {
	u8 checksum = 0;
	u8 idx;
	(void)dummy;
	AMG88_ConvertData(); // from last cycle
	AMG88_ReadData();    // start new cycle
	t_min = AMG88_GetTempMin();
	t_max = AMG88_GetTempMax();
	t_thermistor = AMG88_GetThermistorTemp();
	//
	UART_Transmit(0, (u8*)"***", 3);
	while (!UART_IsTransmitFinished(0));
	UART_Transmit(0, (u8*)&t_thermistor, 2);
	checksum += ((u8*)&t_thermistor)[0];
	checksum += ((u8*)&t_thermistor)[1];
	while (!UART_IsTransmitFinished(0));
	UART_Transmit(0,(u8*)amg88_data,sizeof(amg88_data));
	for (idx=0;idx<sizeof(amg88_data);idx++)
		checksum += ((u8*)amg88_data)[idx];
	while (!UART_IsTransmitFinished(0));
	UART_Transmit(0, &checksum, 1);
	//
#if 1
	int pos, temp, tfract;
	temp = t_max;
	tfract = temp>>2;
	int2str(tfract, str_buffer);
	pos = strlen(str_buffer);
	str_buffer[pos++]= '.';
	temp -= tfract<<2;
	int2str((temp*10)>>2, &str_buffer[pos++]);
	pos = strlen(str_buffer);
	//
	draw_string(4,66,str_buffer, SSD1331_COLOR_WHITE,SSD1331_COLOR_GRAY);
#endif
	draw_ir_rot(0,0);
}


/** 1000ms low priority task
	@param dummy unused parameter needed for Prio_Wrap_Func_t
*/
void task_1000ms(u32 dummy) {
	(void)dummy;
}

void SysTick_Handler(void) {
	PrioWrap_Function(task_1ms, 0);
	if (ms_counter % 10 == 0)
		PrioWrap_Function(task_10ms, 0);
	if (ms_counter % 100 == 0)
		PrioWrap_Function(task_100ms, 0);
	if (ms_counter % 1000 == 0)
		PrioWrap_Function(task_1000ms, 0);

	ms_counter++;
}


int main(void) {

	SystemInit();
	SYSTIME_Init();

	DMA_Init();
	DMA_IRQEnable(1);

	// Init UART
	UART_SetBaudRate(0, /*38400*/115200);
	UART_Setup(0);

	// Init SPI
	SPI_Init();

	// Init I2C
	I2C_Init();

	PIN_Init();
	// disable switch matrix and IO config to save power
	SYSCON_DisableClockControl(SYS_CLK_SWM|SYS_CLK_IOCON);

	// Init OLED
	SSD1331_Init();
	SSD1331_FillScreen(0);
	//SSD1331_FillScreenG(BGCOL1,BGCOL2);
	//Gradient_Create16(BGCOL1, BGCOL2, SSD1331_SIZE_Y-1, &gb);
	//draw_string(4,4,"Hallo!", SSD1331_COLOR_WHITE,SSD1331_COLOR_GRAY);

	AMG88_ReadData();

	// Init timebase
	// SysTick and PendSV are system exceptions - they don't need to be enabled
	PrioWrap_Init();
	NVIC_SetPriority(SysTick_IRQn, SYSTICK_IRQ_PRIO);   /* set to medium prio */
	SYSTICK_Init();                                     /* 1ms system tick */
	SYSTICK_Enable(1);
	SYSTICK_EnableIRQ(1);


	while(1) {
		/* Sleep until next IRQ happens */
		__WFI();
	}
	return 0 ;
}
