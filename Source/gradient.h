/** RGB Gradient library
	Helper function for 16bit (RGB 5-6-5) and 24bit graphics display
	LPC82x ARM Cortex M0+
	----------------------------------------------------------
	Copyright 2015 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

#ifndef GRADIENT_H
#define GRADIENT_H

typedef struct {
	s32 dr;  ///! delta for red component
	s32 dg;  ///! delta for green component
	s32 db;  ///! delta for blue component
	u8  r0;  ///! init value for red component
	u8  g0;  ///! init value for green component
	u8  b0;  ///! init value blue component
} gradient_t;

#define RGB16(r,g,b) ((u16)(( (((r)&31)<<11)) | (((g)&63)<<5) | ((b)&31)  ))

// WS2811 uses RGB format
#define RGB24(r,g,b) (u32)( (((u32)((r)&0xff))<<16) | (((u32)((g)&0xff))<<8) | ((u32)((b)&0xff)) )

// WS2812B uses GRB format
#define GRB24(r,g,b) (u32)( (((u32)((g)&0xff))<<16) | (((u32)((r)&0xff))<<8) | ((u32)((b)&0xff)) )


extern void Gradient_Create16(u16 col1, u16 col2, u16 steps, gradient_t *grad);
extern u16 Gradient_GetColor16(gradient_t *grad, u16 step);

extern void Gradient_Create24(u32 col1, u32 col2, u16 steps, gradient_t *grad);
extern u32 Gradient_GetColor24(gradient_t *grad, u16 step);


#endif
