/** SSD1331 OLED Library
	Communication through 4-line SPI mode
	LPC82x ARM Cortex M0+
	----------------------------------------------------------
	Copyright 2017 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

#include "global.h"
#include "sys.h"
#include "prc.h"
#include "pin.h"
#include "spi.h"
#include "systime.h"
#include "fonts.h"
#include "ssd1331.h"
#include "ssd1331_cnf.h"
#include <string.h>
#include "gradient.h"

/** To speed up text drawing, allocate one scanline in RAM */
u16 ssd1331_scanline[SSD1331_SIZE_X];
/** dummy rx buffer to avoid issue with stack variables */
static u16 col_buf_pix;
static u16 col_buf_fill;
static u8  dummy_rx_buffer[2];
static u8  tx_data[10];

#if !SSD1331_LANDSCAPE_MODE
#define SSD1331_SwapXY(x,y) {u16 stmp; 	stmp = x; x = y;y = stmp; }
#else
#define SSD1331_SwapXY(x,y)
#endif


/** Lookup table for 8bit commands.
 *  Note: this might seem futile on 1st look, but this const table can be used translate a command to a fixed address containing this command.
 *  This makes it possible to enqueue commands with only a pointer to the TX buffer known.
 */
const u8 ssd1331_cmd[256] = {
	  0,   1,  2,    3 ,  4,   5,   6,   7,   8,   9,  10,  11,  12,  13,  14,  15,  16,  17,  18,  19,  20,  21,  22,  23,  24,  25,  26,  27,  28,  29,  30,  31,
	 32,  33,  34,  35,  36,  37,  38,  39,  40,  41,  42,  43,  44,  45,  46,  47,  48,  49,  50,  51,  52,  53,  54,  55,  56,  57,  58,  59,  60,  61,  62,  63,
	 64,  65,  66,  67,  68,  69,  70,  71,  72,  73,  74,  75,  76,  77,  78,  79,  80,  81,  82,  83,  84,  85,  86,  87,  88,  89,  90,  91,  92,  93,  94,  95,
	 96,  97,  98,  99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127,
	128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159,
	160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191,
	192, 193, 194, 195, 196, 197, 198, 199, 200, 201, 202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212, 213, 214, 215, 216, 217, 218, 219, 220, 221, 222, 223,
	224, 225, 226, 227, 228, 229, 230, 231, 232, 233, 234, 235, 236, 237, 238, 239, 240, 241, 242, 243, 244, 245, 246, 247, 248, 249, 250, 251, 252, 253, 254, 255
};

void SSD1331_Callback_Command_Start(void) {
	PIN_ChSet(SSD1331_DC_PIN,0);
}

void SSD1331_Callback_Data_Start(void) {
	PIN_ChSet(SSD1331_DC_PIN,1);
}

/**
 * Write command without parameters
 * @param cmd command
 */
void SSD1331_WriteCommand(u8 cmd) {
	// send command
	while (SPI_GetChPendingCnt(SSD1331_SPI_CH));
	/* Note SPI_CS_HOLD is just used because EOT is used in config anyway!!! No DMA linking needed! */
	SPI_StartTransferX(SSD1331_SPI_CH, (u8*)&ssd1331_cmd[cmd], dummy_rx_buffer, 1, 0, 0, SPI_CS_HOLD, &SSD1331_Callback_Command_Start, NULL);
}

/**
 * Write number of commands and parameters
 * @param commands pointer to commands and parameters
 * @param len number of bytes
 */
void SSD1331_WriteCommands(u8 *commands, u16 len) {
	while (SPI_GetChPendingCnt(SSD1331_SPI_CH));
	SPI_StartTransferX(SSD1331_SPI_CH, commands, dummy_rx_buffer, len, 1, 0, SPI_CS_HOLD, &SSD1331_Callback_Command_Start, NULL);
}

/**
 * Write number of data bytes
 * @param data pointer to data
 * @param len number of bytes
 */
void SSD1331_WriteData(u8 *data, u16 len) {
	while (SPI_GetChPendingCnt(SSD1331_SPI_CH));
	SPI_StartTransferX(SSD1331_SPI_CH, data, dummy_rx_buffer, len, 1, 0, SPI_CS_HOLD, &SSD1331_Callback_Data_Start, NULL);
}

/**
 * Set area where the controller can access the graphics buffer
 * @param x1 1st column to access
 * @param y1 1st line/page to access
 * @param x2 last column to access
 * @param y1 last line/page to access
 */
void SSD1331_SetArea(u16 x1, u16 y1, u16 x2, u16 y2) {
	SSD1331_SwapXY(x1,y1);
	SSD1331_SwapXY(x2,y2);
	tx_data[0] = SSD1331_CMD_COL_ADR;
	tx_data[1] = x1;
	tx_data[2] = x2;
	tx_data[3] = SSD1331_CMD_ROW_ADR;
	tx_data[4] = y1;
	tx_data[5] = y2;
	SSD1331_WriteCommands(&tx_data[0],6);
}

/**
 * Set area where the controller can access the graphics buffer to full screen
 */
void SSD1331_SetFullScreen(void) {
	SSD1331_SetArea(0,0,SSD1331_SIZE_X-1,SSD1331_SIZE_Y-1);
}

/**
 * Draw a single pixel on the screen
 */
void SSD1331_DrawPixel(u16 x, u16 y, u16 color) {
	SSD1331_SetArea(x, y, x, y);
	col_buf_pix = PRC_ReverseByteOrderW(color);
	SSD1331_WriteData((u8*)&col_buf_pix,2);
}

/**
 * Fill a rectangle with the given color
 * @param x1 x coordinate of upper left corner
 * @param y1 y coordinate of upper left corner
 * @param x2 x coordinate of lower right corner
 * @param y1 y coordinate of lower right corner
 * @param color 16bit color to use for filling
 */
void SSD1331_FillRect(u16 x1, u16 y1, u16 x2, u16 y2, u16 color) {
	u16 x,y;
	u16 dx = 1+x2-x1;
	u16 dy = 1+y2-y1;
	SSD1331_SetArea(x1, y1, x2, y2);
	// create scanline
	u16 col_rev = PRC_ReverseByteOrderW(color);
	for (x=0; x<dx; x++)
		ssd1331_scanline[x] = col_rev;
	// write data
	for (y=0; y<dy; y++)
		SSD1331_WriteData((u8*)&ssd1331_scanline, dx*2);
}

/**
 * Fill a rectangle with the given color gradient from color1 to color2
 * @param x1 x coordinate of upper left corner
 * @param y1 y coordinate of upper left corner
 * @param x2 x coordinate of lower right corner
 * @param y1 y coordinate of lower right corner
 * @param color1 16bit color to use as start color for gradient
 * @param color2 16bit color to use as end color for gradient
 */
void SSD1331_FillRectG(u16 x1, u16 y1, u16 x2, u16 y2, u16 color1, u16 color2) {
	u16 x,y;
	u16 dx = 1+x2-x1;
	u16 dy = 1+y2-y1;
	SSD1331_SetArea(x1, y1, x2, y2);
	gradient_t g;
	Gradient_Create16(color1, color2, dy-1, &g);
	// write data
	for (y=0; y<dy; y++) {
		// create scanline
		col_buf_fill = PRC_ReverseByteOrderW(Gradient_GetColor16(&g, y));
		for (x=0; x<dx; x++)
			ssd1331_scanline[x] = col_buf_fill;
		SSD1331_WriteData((u8*)&ssd1331_scanline, dx*2);
	}
}

/**
 * Fill screen with given color
 * @param color 16bit color to use for filling
 */
void SSD1331_FillScreen(u16 color) {
	SSD1331_FillRect(0,0,SSD1331_SIZE_X-1,SSD1331_SIZE_Y-1, color);
}

/**
 * Fill a rectangle with the given color gradient from color1 to color2
 * @param color1 16bit color to use as start color for gradient
 * @param color2 16bit color to use as end color for gradient

 */
void SSD1331_FillScreenG(u16 color1, u16 color2) {
	SSD1331_FillRectG(0,0,SSD1331_SIZE_X-1,SSD1331_SIZE_Y-1, color1, color2);
}

/**
 * Fill a rectangle with the given data
 * @param x1 x coordinate of upper left corner
 * @param y1 y coordinate of upper left corner
 * @param x2 x coordinate of lower right corner
 * @param y1 y coordinate of lower right corner
 * @param data data buffer containing 16bit colors
 */
void SSD1331_BlitRect(u16 x1, u16 y1, u16 x2, u16 y2, u16 *data) {
	SSD1331_SetArea(x1, y1, x2, y2);
	u32 len = (1+x2-x1)*(1+y2-y1)*2;
	u32 ofs = 0;
	while (len > SSD1331_MAX_TRANSFER) {
		SSD1331_WriteData((u8*)&data[ofs],(u16)0xffff);
		ofs += SSD1331_MAX_TRANSFER;
		len -= SSD1331_MAX_TRANSFER;
	}
	SSD1331_WriteData((u8*)&data[ofs],(u16)len);
}

/**
 * Draw a string on the screen
 * @param x x coordinate of upper left corner
 * @param y y coordinate of upper left corner
 * @param str string to print
 * @param font font to use
 * @param fg_col foreground color
 * @param bg_col background color
 */
void SSD1331_DrawStr(u16 x, u16 y, char* str, font_t *font, u16 fg_col, u16 bg_col) {
	u16 x_idx, y_idx, c_idx, dataw, xofs;
	u16 strl = strlen(str);
	u16 line_pixels = strl*font->width;

	fg_col = PRC_ReverseByteOrderW(fg_col);
	bg_col = PRC_ReverseByteOrderW(bg_col);

	SSD1331_SetArea(x, y, x+line_pixels-1, y+font->height-1);
	// go through text line by line
	for (y_idx=0; y_idx<font->height; y_idx++) {
		// wait for line do finish
		while (SPI_GetChPendingCnt(SSD1331_SPI_CH));
		xofs=0;
		// go through text character by character
		for (c_idx=0; c_idx<strl; c_idx++) {
			u8 c = str[c_idx]; // get character
			dataw = font->data[(c - 32) * font->height + y_idx]; // get data word
			// go through character column by column
			for (x_idx=0; x_idx<font->width; x_idx++) {
				ssd1331_scanline[xofs++] = ((dataw & 0x8000)!=0) ? fg_col : bg_col;
				dataw <<= 1;
			}
		}
		// blit line to screen
		SSD1331_WriteData((u8*)&ssd1331_scanline, line_pixels*2);
	}
}

/**
 * Draw a string with gradient on the screen
 * @param x x coordinate of upper left corner
 * @param y y coordinate of upper left corner
 * @param str string to print
 * @param font font to use
 * @param fg_col1 foreground color 1
 * @param fg_col2 foreground color 2
 * @param bg_col1 background color 1
 * @param bg_col2 background color 2
 */
void SSD1331_DrawStrG(u16 x, u16 y, char* str, font_t *font, u16 fg_col1, u16 fg_col2, u16 bg_col1, u16 bg_col2) {
	u16 x_idx, y_idx, c_idx, dataw, xofs, fg_col, bg_col;
	u16 strl = strlen(str);
	u16 line_pixels = strl*font->width;

	gradient_t gf,gb;
	Gradient_Create16(fg_col1, fg_col2, font->height-1, &gf);
	Gradient_Create16(bg_col1, bg_col2, font->height-1, &gb);

	SSD1331_SetArea(x, y, x+line_pixels-1, y+font->height-1);
	// go through text line by line
	for (y_idx=0; y_idx<font->height; y_idx++) {
		// wait for line do finish
		while (SPI_GetChPendingCnt(SSD1331_SPI_CH));
		xofs=0;
		fg_col = PRC_ReverseByteOrderW(Gradient_GetColor16(&gf, y_idx));
		bg_col = PRC_ReverseByteOrderW(Gradient_GetColor16(&gb, y_idx));
		// go through text character by character
		for (c_idx=0; c_idx<strl; c_idx++) {
			u8 c = str[c_idx]; // get character
			dataw = font->data[(c - 32) * font->height + y_idx]; // get data word
			// go through character column by column
			for (x_idx=0; x_idx<font->width; x_idx++) {
				ssd1331_scanline[xofs++] = ((dataw & 0x8000)!=0) ? fg_col : bg_col;
				dataw <<= 1;
			}
		}
		// blit line to screen
		SSD1331_WriteData((u8*)&ssd1331_scanline, line_pixels*2);
	}
}

/**
 * Initialize the display.
 */
void SSD1331_Init(void) {
	u8  param;

	PIN_ChSet(SSD1331_RESET_PIN,0);
	SYSTIME_WaitUs(5); // datasheet requires at least 3µs
	PIN_ChSet(SSD1331_RESET_PIN,1);
	SYSTIME_WaitUs(120000); // datasheet requires 100ms after VCC is stable

	// Initialization Sequence
	SSD1331_WriteCommand(SSD1331_CMD_DISP_OFF);
	SSD1331_WriteCommand(SSD1331_CMD_REMAP);
#if SSD1331_LANDSCAPE_MODE
	param = SSD1331_REMAP_H_INC;
#else
	param = SSD1331_REMAP_V_INC;
#endif
#if SSD1331_FLIPPED_MODE_X
	param |= SSD1331_REMAP_FLIP_COL;
#endif
#if SSD1331_FLIPPED_MODE_Y
	param |= SSD1331_REMAP_COM_SCAN_DIR;
#endif
	SSD1331_WriteCommand(SSD1331_REMAP_COLOR_65K|SSD1331_REMAP_COM_SPLIT_EN|param);
	SSD1331_WriteCommand(SSD1331_CMD_DISP_OFS_ROW);
	SSD1331_WriteCommand(0x0);
	SSD1331_WriteCommand(SSD1331_CMD_DISP_OFS_COL);
	SSD1331_WriteCommand(0x0);
	SSD1331_WriteCommand(SSD1331_CMD_DISP_NORMAL);
	//SSD1331_WriteCommand(SSD1331_CMD_MULTIPLEX);
	//SSD1331_WriteCommand(0x3F);
	SSD1331_WriteCommand(SSD1331_CMD_MASTER_CONFIG);
	SSD1331_WriteCommand(0x8E);
	SSD1331_WriteCommand(SSD1331_CMD_POWER_MODE);
	SSD1331_WriteCommand(0x0B);
	SSD1331_WriteCommand(SSD1331_CMD_PERIOD_ADJUST);
	SSD1331_WriteCommand(0x31);
	SSD1331_WriteCommand(SSD1331_CMD_CLOCKDIV);
	SSD1331_WriteCommand(0xF0);  // 7:4 = Oscillator Frequency, 3:0 = CLK Div Ratio (A[3:0]+1 = 1..16)
	SSD1331_WriteCommand(SSD1331_CMD_PRECHARGEA);
	SSD1331_WriteCommand(0x64);
	SSD1331_WriteCommand(SSD1331_CMD_PRECHARGEB);
	SSD1331_WriteCommand(0x78);
	SSD1331_WriteCommand(SSD1331_CMD_PRECHARGEC);
	SSD1331_WriteCommand(0x64);
	SSD1331_WriteCommand(SSD1331_CMD_PRECHARGE_LEVEL);
	SSD1331_WriteCommand(0x3A);
	SSD1331_WriteCommand(SSD1331_CMD_VCOMH);
	SSD1331_WriteCommand(0x3E);
	SSD1331_WriteCommand(SSD1331_CMD_ATTENTUATION);
	SSD1331_WriteCommand(0x06);
	SSD1331_WriteCommand(SSD1331_CMD_CONTRASTA);
	SSD1331_WriteCommand(0x91);
	SSD1331_WriteCommand(SSD1331_CMD_CONTRASTB);
	SSD1331_WriteCommand(0x50);
	SSD1331_WriteCommand(SSD1331_CMD_CONTRASTC);
	SSD1331_WriteCommand(0x7D);
	SSD1331_WriteCommand(SSD1331_CMD_DISP_ON);
}
