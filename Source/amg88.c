/** AMG88 Library
	AMG88/Grid-Eye library
	LPC824 ARM Cortex M0+
	----------------------------------------------------------
	Copyright 2017 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

#include "global.h"
#include "sys.h"
#include "prc.h"
#include "pin.h"
#include "i2c.h"
#include "systime.h"
#include "amg88.h"

u16 amg88_data[AMG88_PIXEL_NUM];
s16 amg88_temp[AMG88_PIXEL_NUM];
u16 amg88_thermistor_data;
u8  amg88_data_read[2];
u8  amg88_data_write[2];
s16 amg88_temp_max, amg88_temp_min;



/** Start 8bit write transfer
	@return I2C OK Status (see I2C_MST_TRANSFER_xxx)
 */
u8 AMG88_Write8(u8 reg, u8 value) {
	amg88_data_write[0] = value;
	return I2C_MasterTransfer(I2C_CH_AMG88, I2C_TYPE_MST_WRITE, reg, &amg88_data_write[0], 1, NULL);
}


/** Start 16bit write transfer
	@return I2C OK Status (see I2C_MST_TRANSFER_xxx)
 */
u8 AMG88_Write16(u8 reg, u16 value) {
	amg88_data_write[0] = (u8)(value >> 8);
	amg88_data_write[1] = (u8)value;
	return I2C_MasterTransfer(I2C_CH_AMG88, I2C_TYPE_MST_WRITE, reg, &amg88_data_write[0], 2, NULL);
}

/** Read 8bit
	@return value read
 */
u8 AMG88_Read8(u8 reg) {
	I2C_MasterTransfer(I2C_CH_AMG88, I2C_TYPE_MST_READ, reg, &amg88_data_read[0], 1, NULL);
	while(I2C_GetChannelPendingCount(I2C_CH_AMG88)>0);
	return amg88_data_read[0];
}

/** Read 16bit
	@return value read
 */
u16 AMG88_Read16(u8 reg) {
	I2C_MasterTransfer(I2C_CH_AMG88, I2C_TYPE_MST_READ, reg, &amg88_data_read[0], 2, NULL);
	while(I2C_GetChannelPendingCount(I2C_CH_AMG88)>0);
	return ((u16)amg88_data_read[0]<<8) | (u16)amg88_data_read[1];
}

/** Read n bytes
	@return I2C OK Status (see I2C_MST_TRANSFER_xxx)
 */
u8 AMG88_StartRead(u8 reg, u8 *buffer, u8 len) {
	return I2C_MasterTransfer(I2C_CH_AMG88, I2C_TYPE_MST_READ, reg, buffer, len, NULL);
}


void AMG88_ReadData(void) {
	AMG88_StartRead(AMG88_CMD_TTHL, (u8*)&amg88_thermistor_data, sizeof(amg88_thermistor_data));
	AMG88_StartRead(AMG88_CMD_T01L, (u8*)amg88_data, sizeof(amg88_data));
}

void AMG88_ConvertData(void) {
	s16 min = 0x7fff;
	s16 max = -0x8000;
	s16 temp;
	int i;

	for (i=0; i<AMG88_PIXEL_NUM; i++) {
		temp = (s16)amg88_data[i];
		if (temp & 0x800)
			temp = (s16)(temp&0x7ff)-0x800;
		amg88_temp[i] = temp;
		if (temp < min)
			min = temp;
		if (temp > max)
			max = temp;
	}
	amg88_temp_max = max;
	amg88_temp_min = min;
}

/** Read thermistor temperature.
	@return temperature in 0.0625°C resolution
 */
s16 AMG88_GetThermistorTemp(void) {
	if (amg88_thermistor_data & 0x800)
		return (s16)(amg88_thermistor_data&0x7ff)-0x800;
	else
		return (s16)amg88_thermistor_data;
}


/** Read pixel temperature.
	@return temperature 256=1°C
 */
s16 AMG88_GetTemp(u8 x, u8 y) {
	return amg88_temp[x*+y*8];
}

/** Read maximum temperature high.
	@return temperature 256=1°C
 */
s16 AMG88_GetTempMax(void) {
	return amg88_temp_max;
}

/** Read minimum temperature.
	@return temperature 256=1°C
 */
s16 AMG88_GetTempMin(void) {
	return amg88_temp_min;
}

void AMG88_WaitForCompletion(void) {
	while(I2C_GetChannelPendingCount(I2C_CH_AMG88)>0);
}
